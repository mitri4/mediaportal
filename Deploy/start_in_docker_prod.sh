#!/usr/bin/env bash

set -e
set -u

export DJANGO_SETTINGS_MODULE=Project.production_settings

cd /opt/app
python3 manage.py migrate --noinput        # Apply database migrations
python3 manage.py collectstatic --noinput  # Collect static files
python3 manage.py createdefaultuser   # Create default user admin

supervisord -c /opt/app/Deploy/supervisor.conf
