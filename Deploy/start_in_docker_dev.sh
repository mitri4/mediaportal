#!/usr/bin/env bash

set -e
set -u

export DJANGO_SETTINGS_MODULE=Project.develop_settings

cd /opt/app
python3 manage.py migrate --noinput        # Apply database migrations
python3 manage.py collectstatic --noinput  # Collect static files
python3 manage.py createdefaultuser   # Create default user admin

celery -A Project worker -l info  --concurrency=4 &

python3 manage.py runserver 0.0.0.0:8000
