import pytest

from apps.base import common


class TestIsString:
    @pytest.mark.parametrize('value', [
        'Test',
        '13,24',
        '13243.qe13243r',
        'http://videolink.ru/1',
    ])
    def test_is_string(self, value):
        assert common.is_string(value) is True

    @pytest.mark.parametrize('value', [
        '12.1',
        '1343',
        '1',
    ])
    def test_is_not_string(self, value):
        assert common.is_string(value) is False
