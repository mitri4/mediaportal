import json
import pytest

from django.urls import reverse
from rest_framework import status

from apps.cms import models


@pytest.mark.django_db
def test_list(api_client):
    """
    Test page list
    """
    url = reverse('cms:page-list')
    response = api_client.get(
        url,
        status=status.HTTP_200_OK
    )
    assert json.loads(response.content)['count'] == 12
    assert json.loads(response.content)['next'].split('?')[1] == 'page=2'
    api_client.post(
        url,
        status=status.HTTP_405_METHOD_NOT_ALLOWED
    )


@pytest.mark.celery(result_backend='redis://')
@pytest.mark.django_db
def test_detail(api_client):
    """
    Test page list
    """
    url = reverse('cms:page-detail', args=(1,))
    response = api_client.get(
        url,
        status=status.HTTP_200_OK
    )
    assert json.loads(response.content)['pk'] == 1
    # counter check
    content_item = json.loads(response.content)['content'][0]['pk']
    assert models.Content.objects.get(pk=content_item).counter == 1
