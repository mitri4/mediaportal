import pytest

from apps.cms import models


@pytest.mark.django_db
def test_content_order():
    page = models.Page.objects.first()
    page.is_random_content = False
    page.save()
    content_on_page = page.content
    first_content = content_on_page[0]
    assert first_content.position == 1
    page.is_random_content = True
    page.save()
    new_content_on_page = page.content
    assert not new_content_on_page == content_on_page
