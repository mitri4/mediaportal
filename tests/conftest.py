import pytest

from tests.client import BetterAPIClient


@pytest.fixture
def api_client():
    """Anonymous client for REST API."""
    client = BetterAPIClient()
    return client
