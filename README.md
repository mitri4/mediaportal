MediaPortal
===
Simple API for media portal 

Installation
===
    git clone https://gitlab.com/mitri4/mediaportal.git
    
Local develop
===
For local develop you need install rabbitmq and postgresql

Install requirements
---
    cd mediaportal
    pip install -r requirements/dev.txt

Run celery worker
---
    celery -A Project worker -l info

Make migrations
---
__NOTE: Before  make migration or run develop server create database or change connection database in `./Project/settings.py`__  
__NOTE: With migration create demo data__

    python3 manage.py migrate

Run develop server
---
    python3 manage.py runserver

Create default user
---
Create user `admin@media-portal.com` with password `admin`
    
    python3 manage.py createdefaultuser

Develop with docker-compose
====
For run  develop server on 0.0.0.0:8000 use command:
    
    docker-compose up runserver
    
Test code
===
Local test
---
For run test use command
 
    pytest
    
Test in docker
---
Run command 

    docker-compose up autotests

Use in production
===
For use app in production run command:

    docker-compose -f docker-compose.prod.yml -d --build

Routing
===

* `GET /admin/` - Admin panel
* `GET /api/v1/pages/` - Pages list
* `GET /api/v1/pages/:page_id/` - Page detail for page with id `page_id`
* `GET /status/` - Health Status application
* `GET /docs/` - Swagger API Documentation

Settings
===
* `./Project/setting.py` - use for local develop
* `./Project/develop_setting.py` - use for develop id Docker
* `./Project/production_settings.py` -  use for production server in Docker
* `./Project/production_qa.py` -  use for testing code in Docker and local

API Documentation
===
For the validation of methods of API use Swagger
See on url `/docs/`.

Models
===
![Models](./docs/models.png)

Requirements
=====
- python 3.6+
- Django 2.1+
- Django Rest Framework
- PostgreSQL
- Celery
- Docker
- docker-compose

Contributors
=====
- [mitri4](https://gitlab.com/mitri4)

License
=====
MediaPortal is released under the MIT License. See the LICENSE file for more details.
