from apps.cms.models import Content
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Reset count view content.'

    def handle(self, *args, **options):
        self.reset_counter()

    def reset_counter(self):
        Content.objects.all().update(counter=0)
        message = f'[{__name__}] Counter content successfully reset.'
        self.stdout.write(self.style.SUCCESS(message))
