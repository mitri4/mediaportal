from celery import task

from django.db.models import F

from . import models


@task
def page_viewed(page_id):
    page = models.Page.objects.get(pk=page_id)
    content_list = page.content.values_list('content_id', flat=True)
    models.Content.objects.filter(pk__in=content_list).update(counter=F('counter') + 1)
