from rest_framework import serializers

from . import models


class AttributesForContentSerializer(serializers.ModelSerializer):
    value = serializers.ReadOnlyField(source='get_value')
    title = serializers.ReadOnlyField(source='attribute.attribute.title')

    class Meta:
        model = models.AttributesForContent
        fields = (
            'title',
            'value',
        )


class ContentOnPageSerializer(serializers.ModelSerializer):
    title = serializers.ReadOnlyField(source='content.title')
    type = serializers.ReadOnlyField(source='content.type.title')
    attributes = AttributesForContentSerializer(many=True, read_only=True)

    class Meta:
        model = models.ContentOnPage
        fields = (
            'pk',
            'title',
            'type',
            'attributes',
            'position'
        )


class PageSerializer(serializers.ModelSerializer):
    content = ContentOnPageSerializer(many=True, read_only=True)

    class Meta:
        model = models.Page
        fields = (
            'pk',
            'title',
            'slug',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'content',
        )


class PageListSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='cms:page-detail',
        lookup_field='pk',
        read_only=True
    )

    class Meta:
        model = models.Page
        fields = (
            'url',
            'pk',
            'title',
            'slug',
        )
