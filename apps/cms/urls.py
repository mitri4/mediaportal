from django.urls import path

from . import views

urlpatterns = [
    path('pages/', views.PageListView.as_view(), name='page-list'),
    path('pages/<int:pk>/', views.PageDetailView.as_view(), name='page-detail'),
]
