from rest_framework import generics

from . import (models, serializers, signals)


class PageListView(generics.ListAPIView):
    """
    List of pages
    ---
    Use this endpoint for get pages list.
    Pages with the is_show flag are displayed.
    Pagination of 10 pages.
    """
    model = models.Page
    serializer_class = serializers.PageListSerializer
    queryset = models.Page.objects.showed()


class PageDetailView(generics.RetrieveAPIView):
    """
    Page info.
    ---
    Use this endpoint for get page info.
    """
    model = models.Page
    serializer_class = serializers.PageSerializer
    queryset = models.Page.objects.showed()

    def get(self, request, *args, **kwargs):
        if 'pk' in kwargs:
            try:
                page = self.queryset.get(pk=kwargs['pk'])
                signals.content_viewed.send(
                    sender=self.__class__,
                    page=page,
                    request=self.request
                )
            except models.Page.DoesNotExist:
                pass
        return self.retrieve(request, *args, **kwargs)
