from django.db import models

from apps.base.models import TitleMixin, SeoMixin
from . import managers

FIELD_TYPE_CHAR = 0
FIELD_TYPE_INT = 1
FIELD_TYPE_DECIMAL = 2
FIELD_TYPE_TEXT = 3

FIELD_TYPE_CHOICES = (
    (FIELD_TYPE_CHAR, 'Char(256)'),
    (FIELD_TYPE_INT, 'Integer'),
    (FIELD_TYPE_DECIMAL, 'Decimal'),
    (FIELD_TYPE_TEXT, 'Text'),
)


class Page(TitleMixin, SeoMixin):
    slug = models.SlugField(max_length=256, default='')
    is_show = models.BooleanField(default=True, help_text='Show page on site.')
    is_random_content = models.BooleanField(
        default=False,
        help_text='Random ordering content on page.'
    )

    objects = managers.PageManager()

    class Meta:
        db_table = 'pages'
        verbose_name = 'page'
        verbose_name_plural = 'pages'

    @property
    def content(self):
        if self.is_random_content:
            return self.for_content.all().order_by('?')
        return self.for_content.all().order_by('position')


class Content(TitleMixin):
    type = models.ForeignKey(
        'TypeContent',
        related_name='type_content',
        on_delete=models.CASCADE
    )
    counter = models.PositiveIntegerField(default=0, editable=False)

    class Meta:
        db_table = 'contents'
        verbose_name = 'content'
        verbose_name_plural = 'contents'


class TypeContent(TitleMixin):
    attributes = models.ManyToManyField('Attribute', through='AttributeForTypeContent')

    class Meta:
        db_table = 'type_contents'
        verbose_name = 'content type'
        verbose_name_plural = 'content types'


class Attribute(TitleMixin):
    slug = models.SlugField(max_length=256, default='')

    class Meta:
        db_table = 'attributes'
        verbose_name = 'attribute'
        verbose_name_plural = 'attributes'


class AttributeForTypeContent(models.Model):
    type_content = models.ForeignKey(
        TypeContent,
        related_name='for_attribute',
        on_delete=models.CASCADE
    )
    attribute = models.ForeignKey(
        Attribute,
        related_name='for_type_content',
        on_delete=models.CASCADE
    )
    field_type = models.PositiveSmallIntegerField(
        choices=FIELD_TYPE_CHOICES,
        default=FIELD_TYPE_CHAR,
    )
    position = models.PositiveIntegerField(default=0)

    class Meta:
        db_table = 'attributes_for_type_content'
        ordering = ('position',)
        verbose_name = 'attribute for content type'
        verbose_name_plural = 'attributes for content type'

    def __str__(self):
        return f'{self.attribute.title} ({self.type_content.title})'


class AttributesForContent(models.Model):
    attribute = models.ForeignKey(
        AttributeForTypeContent,
        related_name='for_content',
        on_delete=models.CASCADE)
    content = models.ForeignKey(
        'Content',
        related_name='content_attributes',
        on_delete=models.CASCADE
    )
    value = models.TextField(blank=True, null=True)
    value_char = models.CharField(max_length=256, editable=False, blank=True, null=True)
    value_int = models.IntegerField(editable=False, blank=True, null=True)
    value_decimal = models.FloatField(editable=False, blank=True, null=True)
    value_text = models.TextField(editable=False, blank=True, null=True)

    class Meta:
        db_table = 'attributes_for_content'
        verbose_name = 'attribute for content'
        verbose_name_plural = 'attributes for content'

    def __str__(self):
        return f'{self.attribute}'

    @property
    def field_values(self):
        """
        Get list with fields value_ name
        """
        return [field_name for field_name in self.__dict__.keys() if 'value_' in field_name]

    def set_value(self):
        setattr(self, self.field_values[self.attribute.field_type],
                self.value)
        self.value = None
        self.save()

    def get_value(self):
        return getattr(self, self.field_values[self.attribute.field_type])


class ContentOnPage(models.Model):
    page = models.ForeignKey(
        'Page',
        related_name='for_content',
        on_delete=models.CASCADE
    )
    content = models.ForeignKey(
        'Content',
        related_name='for_page',
        on_delete=models.CASCADE
    )
    position = models.PositiveIntegerField(default=0)

    class Meta:
        db_table = 'contents_on_pages'
        ordering = ('position',)
        verbose_name = 'content on page'
        verbose_name_plural = 'contents on pages'

    def __str__(self):
        return f'{self.content} ({self.page})'

    @property
    def attributes(self):
        return self.content.content_attributes.all()
