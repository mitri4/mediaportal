from django.apps import AppConfig


class CmsConfig(AppConfig):
    name = 'apps.cms'
    verbose_name = 'CMS'

    def ready(self):
        import apps.cms.signals  # noqa
