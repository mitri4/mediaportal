from django.db.models import Manager


class PageManager(Manager):

    def showed(self):
        return self.filter(is_show=True).order_by('-pk')
        # return self.filter(is_show=True).order_by('-created')
