from django.dispatch import Signal, receiver

from . import tasks

# Content on page viewed.
content_viewed = Signal(providing_args=["page", "request"])


@receiver(content_viewed)
def update_counter(sender, page, request, **kwargs):
    tasks.page_viewed.delay(page.id)
