from django.contrib import admin

from adminsortable2.admin import SortableInlineAdminMixin
from django.utils.functional import curry

from . import models, forms


class AttributeForTypeContentInline(admin.TabularInline):
    model = models.AttributeForTypeContent
    extra = 1


class AttributesForContentInline(admin.TabularInline):
    form = forms.AttributesForContentAdminForm
    formset = forms.AttributesForContentFormSet
    model = models.AttributesForContent
    extra = 1

    def get_formset(self, request, obj=None, **kwargs):
        initial = []
        formset = super(AttributesForContentInline, self).get_formset(request, obj, **kwargs)
        formset.__init__ = curry(formset.__init__, initial=initial)
        return formset


class ContentOnPageInline(SortableInlineAdminMixin, admin.TabularInline):
    model = models.ContentOnPage
    extra = 1


@admin.register(models.Page)
class PageAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {'fields': (('title', 'slug'), 'is_show', 'is_random_content')}),
        ('SEO', {
            'fields': ('meta_title', 'meta_keywords', 'meta_description'),
            'classes': ['collapse']
        }),
    )
    list_display = ('title', 'slug', 'is_show', 'is_random_content',)
    prepopulated_fields = {'slug': ('title',)}
    list_filter = ('is_show',)
    search_fields = ('title',)
    inlines = (ContentOnPageInline,)


@admin.register(models.Content)
class ContentAdmin(admin.ModelAdmin):
    inlines = (AttributesForContentInline,)
    search_fields = ('title',)
    list_display = (
        'title',
        'type',
        'counter'
    )

    def get_formsets_with_inlines(self, request, obj=None):
        print("ContentAdmin.get_formsets_with_inlines, obj =", obj)
        return super(ContentAdmin, self).get_formsets_with_inlines(request, obj)


@admin.register(models.TypeContent)
class TypeContentAdmin(admin.ModelAdmin):
    inlines = (AttributeForTypeContentInline,)


@admin.register(models.Attribute)
class AttributeAdmin(admin.ModelAdmin):
    list_display = ('title',)
    prepopulated_fields = {'slug': ('title',)}
