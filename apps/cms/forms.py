from django import forms

from apps.base.common import is_string
from . import models


class AttributesForContentAdminForm(forms.ModelForm):
    value = forms.CharField(
        widget=forms.Textarea(attrs={'rows': '1', 'cols': '60'}),
        required=True
    )

    class Meta:
        model = models.AttributesForContent
        fields = ('attribute', 'value')

    def __init__(self, *args, **kwargs):
        super(AttributesForContentAdminForm, self).__init__(*args, **kwargs)
        if hasattr(self.instance, 'attribute'):
            self.initial['value'] = self.instance.get_value()

    def clean_value(self):
        attribute = self.cleaned_data['attribute']
        value = self.cleaned_data['value']
        if attribute.field_type in (
            models.FIELD_TYPE_INT,
            models.FIELD_TYPE_DECIMAL
        ) and is_string(value):
            raise forms.ValidationError('You cannot set the current value of this parameter!')
        return value


class AttributesForContentFormSet(forms.BaseInlineFormSet):
    def save_new_objects(self, commit=True):
        saved_instances = super(AttributesForContentFormSet, self).save_new_objects(commit)
        if commit:
            for instance in saved_instances:
                # validate content for type
                instance.set_value()
        return saved_instances

    def save_existing_objects(self, commit=True):
        saved_instances = super(AttributesForContentFormSet, self).save_existing_objects(commit)
        if commit:
            for instance in saved_instances:
                # validate content for type
                instance.set_value()
        return saved_instances
