import random

from django.db import migrations


def forwards_func(apps, schema_editor):
    Page = apps.get_model('cms', 'Page')
    Content = apps.get_model('cms', 'Content')
    ContentOnPage = apps.get_model('cms', 'ContentOnPage')
    db_alias = schema_editor.connection.alias
    pages = Page.objects.using(db_alias).all()
    contents = Content.objects.using(db_alias).all()
    for page in pages:
        count = random.randint(1, 9)
        random_content_list = random.sample(list(contents), count)
        for i, random_content_item in enumerate(random_content_list):
            ContentOnPage.objects.using(db_alias).create(
                page=page,
                content=random_content_item,
                position=i + 1
            )


def reverse_func(apps, schema_editor):
    Page = apps.get_model('cms', 'Page')
    ContentOnPage = apps.get_model('cms', 'ContentOnPage')
    db_alias = schema_editor.connection.alias
    pages = Page.objects.using(db_alias).all()
    for page in pages:
        ContentOnPage.objects.using(db_alias).filter(page=page).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_create_data_page'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
