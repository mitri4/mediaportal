from django.db import migrations

from apps.base import common


def forwards_func(apps, schema_editor):
    TypeContent = apps.get_model('cms', 'TypeContent')
    db_alias = schema_editor.connection.alias
    for content_type in common.content_type_list():
        TypeContent.objects.using(db_alias).create(title=content_type.capitalize())


def reverse_func(apps, schema_editor):
    TypeContent = apps.get_model('cms', 'TypeContent')
    db_alias = schema_editor.connection.alias
    for content_type in common.content_type_list():
        TypeContent.objects.using(db_alias).filter(title__iexact=content_type).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0011_create_data_attributes'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
