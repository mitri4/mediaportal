from django.db import migrations

from apps.base import common


def forwards_func(apps, schema_editor):
    Content = apps.get_model('cms', 'Content')
    TypeContent = apps.get_model('cms', 'TypeContent')
    db_alias = schema_editor.connection.alias

    for content_type in common.content_type_list():
        for title in common.content_name_list(content_type)[:3]:
            type_content = TypeContent.objects.using(db_alias).filter(
                title=content_type.capitalize()).first()
            Content.objects.using(db_alias).create(title=title, type=type_content)


def reverse_func(apps, schema_editor):
    Content = apps.get_model('cms', 'Content')
    db_alias = schema_editor.connection.alias
    for content_type in common.content_type_list():
        for title in common.content_name_list(content_type)[:3]:
            Content.objects.using(db_alias).filter(title=title).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_create_data_attribute_for_type_content'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
