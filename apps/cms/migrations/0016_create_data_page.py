import random

from django.db import migrations
from django.utils.text import slugify

from apps.base import common


def forwards_func(apps, schema_editor):
    Page = apps.get_model('cms', 'Page')
    db_alias = schema_editor.connection.alias
    for page_name in common.content_name_list('page'):
        Page.objects.using(db_alias).create(
            title=page_name,
            slug=slugify(page_name),
            meta_title=f'{page_name} meta title',
            meta_keywords='pages, media content, portal, video',
            meta_description='some description text here.',
            is_show=True,
            is_random_content=bool(random.getrandbits(1))
        )


def reverse_func(apps, schema_editor):
    Page = apps.get_model("cms", "Page")
    db_alias = schema_editor.connection.alias
    for page_name in common.content_name_list('page'):
        Page.objects.using(db_alias).filter(title=page_name).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0015_create_data_attributes_for_content'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
