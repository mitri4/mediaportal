from django.db import migrations
from django.utils.text import slugify

from apps.base import common


def forwards_func(apps, schema_editor):
    Attribute = apps.get_model('cms', 'Attribute')
    db_alias = schema_editor.connection.alias
    for attr in common.ATTRIBUTE_LIST:
        Attribute.objects.using(db_alias).create(title=attr, slug=slugify(attr))


def reverse_func(apps, schema_editor):
    Attribute = apps.get_model("cms", "Attribute")
    db_alias = schema_editor.connection.alias
    for attr in common.ATTRIBUTE_LIST:
        Attribute.objects.using(db_alias).filter(title=attr, slug=slugify(attr)).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0010_auto_20180906_0214'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
