from django.db import migrations

from apps.base import common
from apps.cms import models


def forwards_func(apps, schema_editor):
    AttributeForTypeContent = apps.get_model('cms', 'AttributeForTypeContent')
    TypeContent = apps.get_model('cms', 'TypeContent')
    Attribute = apps.get_model('cms', 'Attribute')
    db_alias = schema_editor.connection.alias

    video_type_content = TypeContent.objects.using(db_alias).filter(title='Video').first()
    audio_type_content = TypeContent.objects.using(db_alias).filter(title='Audio').first()
    text_type_content = TypeContent.objects.using(db_alias).filter(title='Text').first()

    AttributeForTypeContent.objects.using(db_alias).bulk_create([
        AttributeForTypeContent(
            type_content=video_type_content,
            attribute=Attribute.objects.using(db_alias).filter(title='Link-video').first(),
            field_type=models.FIELD_TYPE_CHAR
        ),
        AttributeForTypeContent(
            type_content=video_type_content,
            attribute=Attribute.objects.using(db_alias).filter(title='Link-subtitle').first(),
            field_type=models.FIELD_TYPE_CHAR
        ),
        AttributeForTypeContent(
            type_content=audio_type_content,
            attribute=Attribute.objects.using(db_alias).filter(title='Bitrate').first(),
            field_type=models.FIELD_TYPE_INT
        ),
        AttributeForTypeContent(
            type_content=text_type_content,
            attribute=Attribute.objects.using(db_alias).filter(title='Text').first(),
            field_type=models.FIELD_TYPE_TEXT
        )
    ])


def reverse_func(apps, schema_editor):
    AttributeForTypeContent = apps.get_model('cms', 'AttributeForTypeContent')
    TypeContent = apps.get_model('cms', 'TypeContent')
    db_alias = schema_editor.connection.alias

    for content_type in common.content_type_list():
        type_content = TypeContent.objects.using(db_alias).filter(
            title=content_type.capitalize()).first()
        AttributeForTypeContent.objects.using(db_alias).filter(
            type_content=type_content).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0012_create_data_type_content'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
