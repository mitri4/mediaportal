# Generated by Django 2.1.1 on 2018-09-05 11:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0007_auto_20180905_1434'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='is_random_content',
            field=models.BooleanField(default=False),
        ),
    ]
