from django.db import migrations

from apps.base import common


def forwards_func(apps, schema_editor):
    Content = apps.get_model('cms', 'Content')
    Attribute = apps.get_model('cms', 'Attribute')
    TypeContent = apps.get_model('cms', 'TypeContent')
    AttributeForTypeContent = apps.get_model('cms', 'AttributeForTypeContent')
    AttributesForContent = apps.get_model('cms', 'AttributesForContent')
    db_alias = schema_editor.connection.alias

    link_video = Attribute.objects.using(db_alias).filter(title='Link-video').first()
    link_subtitle = Attribute.objects.using(db_alias).filter(title='Link-subtitle').first()
    bitrate = Attribute.objects.using(db_alias).filter(title='Bitrate').first()
    text = Attribute.objects.using(db_alias).filter(title='Text').first()

    video_type_content = TypeContent.objects.using(db_alias).filter(title='Video').first()
    audio_type_content = TypeContent.objects.using(db_alias).filter(title='Audio').first()
    text_type_content = TypeContent.objects.using(db_alias).filter(title='Text').first()

    attr_video_link = AttributeForTypeContent.objects.using(db_alias).filter(
        type_content=video_type_content,
        attribute=link_video,
    ).first()
    attr_video_subtitle = AttributeForTypeContent.objects.using(db_alias).filter(
        type_content=video_type_content,
        attribute=link_subtitle,
    ).first()

    attr_audio_bitrate = AttributeForTypeContent.objects.using(db_alias).filter(
        type_content=audio_type_content,
        attribute=bitrate,
    ).first()

    attr_text_text = AttributeForTypeContent.objects.using(db_alias).filter(
        type_content=text_type_content,
        attribute=text,
    ).first()

    AttributesForContent.objects.using(db_alias).bulk_create([
        AttributesForContent(
            attribute=attr_video_link,
            content=Content.objects.using(db_alias).filter(title='First video').first(),
            value_char='https://youtu.be/oNFNnpxzeH4'
        ),
        AttributesForContent(
            attribute=attr_video_subtitle,
            content=Content.objects.using(db_alias).filter(title='First video').first(),
            value_char='https://youtu.be/oNFNnpxzeH4/subtitle'
        ),
        AttributesForContent(
            attribute=attr_video_link,
            content=Content.objects.using(db_alias).filter(title='Second video').first(),
            value_char='https://youtu.be/U7Zo_e28aQA'
        ),
        AttributesForContent(
            attribute=attr_video_subtitle,
            content=Content.objects.using(db_alias).filter(title='Second video').first(),
            value_char='https://youtu.be/U7Zo_e28aQA/subtitle'
        ),
        AttributesForContent(
            attribute=attr_video_link,
            content=Content.objects.using(db_alias).filter(title='Third video').first(),
            value_char='https://youtu.be/jMZviSKf3QU'
        ),
        AttributesForContent(
            attribute=attr_video_subtitle,
            content=Content.objects.using(db_alias).filter(title='Third video').first(),
            value_char='https://youtu.be/jMZviSKf3QU/subtitle'
        ),
        AttributesForContent(
            attribute=attr_audio_bitrate,
            content=Content.objects.using(db_alias).filter(title='First audio').first(),
            value_int='128'
        ),
        AttributesForContent(
            attribute=attr_audio_bitrate,
            content=Content.objects.using(db_alias).filter(title='Second audio').first(),
            value_int='320'
        ),
        AttributesForContent(
            attribute=attr_audio_bitrate,
            content=Content.objects.using(db_alias).filter(title='Third audio').first(),
            value_int='192'
        ),

        AttributesForContent(
            attribute=attr_text_text,
            content=Content.objects.using(db_alias).filter(title='First text').first(),
            value_text='Very long text'
        ),
        AttributesForContent(
            attribute=attr_text_text,
            content=Content.objects.using(db_alias).filter(title='Second text').first(),
            value_text='Lorem Ipsum is simply dummy text of the printing and typesetting.'
                       'Lorem Ipsum has been the industry standard dummy text ever since'
        ),
        AttributesForContent(
            attribute=attr_text_text,
            content=Content.objects.using(db_alias).filter(title='Third text').first(),
            value_text='Vestibulum ante ipsum primis in faucibus orci luctus et ultrices '
                       'posuere cubilia Curae; Nulla quam quam, ornare vel porttitor non, '
                       'imperdiet.Nulla sodales faucibus sapien, in fermentum lectus.'
        ),
    ])


def reverse_func(apps, schema_editor):
    Content = apps.get_model('cms', 'Content')
    AttributesForContent = apps.get_model('cms', 'AttributesForContent')
    db_alias = schema_editor.connection.alias

    for content_name in ('video', 'audio', 'text'):
        for title in common.content_name_list(content_name)[:3]:
            content = Content.objects.using(db_alias).filter(title=title).first()
            AttributesForContent.objects.using(db_alias).filter(content=content).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0014_create_data_content'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
