from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

EMAIL = 'admin@media-portal.com'
PASSWORD = 'admin'
FIRST_NAME = 'Admin'
LAST_NAME = ''


class Command(BaseCommand):
    help = 'Create default super user'

    def handle(self, *args, **options):
        self.create_user()

    def create_user(self):
        try:
            get_user_model().objects.get(email=EMAIL)
            message = '[{0}] Default django user already exists.'.format(__name__)
            self.stdout.write(self.style.SUCCESS(message))
        except get_user_model().DoesNotExist:
            user = get_user_model().objects.create(
                email=EMAIL,
                first_name=FIRST_NAME,
                last_name=LAST_NAME,
                is_superuser=True,
                is_staff=True
            )
            user.set_password(PASSWORD)
            user.save()
            message = f'[{__name__}] Default django user successfully created.'
            self.stdout.write(self.style.SUCCESS(message))
