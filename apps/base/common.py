ATTRIBUTE_LIST = ('Link-video', 'Link-subtitle', 'Bitrate', 'Text',)


def is_string(string):
    if string.isalpha():
        return True
    if string.isdigit():
        return False
    if string.isalnum():
        return True
    if ',' in string:
        return True
    if '.' in string:
        try:
            float(string)
            return False
        except ValueError:
            return True
    return True


def content_name_list(content_name):
    content_list = (
        f'First {content_name}', f'Second {content_name}', f'Third {content_name}',
        f'Fourth {content_name}', f'Five {content_name}', f'Six {content_name}',
        f'Seven {content_name}', f'Eight {content_name}', f'Nine {content_name}',
        f'Ten {content_name}', f'Eleventh {content_name}', f'Twelve {content_name}',
    )
    return content_list


def content_type_list():
    return 'video', 'audio', 'text',
