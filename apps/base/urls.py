from django.urls import path

from . import views

urlpatterns = [
    path('status/', views.HealthStatusView.as_view(), name='status'),
]
