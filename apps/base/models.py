from django.db import models


class TitleMixin(models.Model):
    title = models.CharField(max_length=256, default='', null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class SeoMixin(models.Model):

    meta_title = models.CharField(max_length=256, default='', null=True, blank=True)
    meta_keywords = models.TextField(default='', null=True, blank=True)
    meta_description = models.TextField(default='', null=True, blank=True)

    class Meta:
        abstract = True
