from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView


class HealthStatusView(APIView):
    def get(self, request, *args, **kwargs):
        payload = {'standby': True}
        return Response(payload, status=status.HTTP_200_OK)
