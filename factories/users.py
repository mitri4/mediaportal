import os
import pytz

import factory
import factory.fuzzy

from functools import partial

from django.conf import settings
from django.contrib.auth import get_user_model


USER_PASSWORD = 'test'

Faker = partial(factory.Faker, locale='ru_RU')

AVATAR_SAMPLE_IMAGE = os.path.join(settings.TEST_IMAGE_DIR, 'simple.jpg')


class UserFactory(factory.django.DjangoModelFactory):
    first_name = Faker('first_name')
    last_name = Faker('last_name')
    email = Faker('email')
    avatar = factory.django.ImageField(from_path=AVATAR_SAMPLE_IMAGE)
    password = factory.PostGenerationMethodCall('set_password', USER_PASSWORD)
    is_active = True
    is_staff = False
    date_joined = Faker(
        'past_datetime',
        start_date='-30d',
        tzinfo=pytz.UTC
    )

    class Meta:
        model = get_user_model()
        django_get_or_create = ('email',)
