from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.schemas import SchemaGenerator
from rest_framework.views import APIView
from rest_framework_swagger import renderers


class SwaggerSchemaView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (
        renderers.SwaggerUIRenderer,
        renderers.OpenAPIRenderer,
    )

    def get(self, request):
        from . import urls

        generator = SchemaGenerator(
            title='Media Portal API Documentation',
            patterns=urls.api_urlpatterns
        )
        schema = generator.get_schema(request=request)

        return Response(schema)
