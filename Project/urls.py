from django.contrib import admin
from django.urls import path, include

from Project.swagger import SwaggerSchemaView

admin.site.site_header = 'Media Portal Admin'
admin.site.site_title = 'Media Admin Portal'
admin.site.index_title = 'Welcome to Media Portal'

api_urlpatterns = [
    path('admin/', admin.site.urls),
    path(
        'api/v1/',
        include(('apps.cms.urls', 'apps.cms'), namespace='cms'),
    ),
    # for swagger login-logout
    path(
        'api/v1/auth/',
        include(('rest_framework.urls', 'rest_framework'), namespace='rest-framework')
    ),
]

doc_urlpatterns = [
    path('', include(('apps.base.urls', 'apps.base'), namespace='base')),
    path('docs/', SwaggerSchemaView.as_view(), name="docs"),

]

urlpatterns = api_urlpatterns + doc_urlpatterns
